package com.techrush.preveenraj.goldrate;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {


    private AdView mAdView;

    TextView goldrate,date;
    String datastring,datestring,timeStamp,ratestring;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        mAdView.loadAd(adRequest);




        goldrate = (TextView) findViewById(R.id.goldrate);
        date = (TextView) findViewById(R.id.date);

        SharedPreferences sharedPreferences = getSharedPreferences("GOLD",MODE_PRIVATE);
        ratestring = sharedPreferences.getString("goldrate","Please Refresh");
        datestring = sharedPreferences.getString("date"," ");
        goldrate.setText(ratestring);
        date.setText(datestring);



    }

    public void OnClick(View view) {

        String time = new SimpleDateFormat("HHmm").format(Calendar.getInstance().getTime());
       // Log.i("time: ",time);

        int timeint = Integer.parseInt(time);


        if(timeint<=1030)
        {
            Toast.makeText(this,"Try after 10:30 am", Toast.LENGTH_SHORT).show();

        }

        else {


            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

            StrictMode.setThreadPolicy(policy);


            final ProgressDialog progress = new ProgressDialog(this);
            progress.setMessage("Wait... ");
            progress.setCancelable(false);


            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {

                        final Document htmldata = Jsoup.connect("http://www.keralagold.com/kerala-gold-rate-per-gram.htm").get();


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                datastring = htmldata.toString();


                                ratestring = datastring.substring(datastring.indexOf("Today")+7);
                                ratestring = ratestring.substring(ratestring.indexOf("Today"));
                                ratestring = ratestring.substring(ratestring.indexOf("Rs. "));
                                ratestring = ratestring.substring(ratestring.indexOf("Rs. "),ratestring.indexOf("<")) + " /gm";


                            //    ratestring = datastring.substring(datastring.indexOf("<font color=\"#C00000\">Today ") + 95, datastring.indexOf("<font color=\"#C00000\">Today ") + 103) + " /gm";
                                Log.i("datastring: ",datastring);


                                goldrate.setText(ratestring);

                                timeStamp = datastring.substring(datastring.indexOf("kg2b") + 6, datastring.indexOf("<br><font color=\"#C00000\">Today "));


                                //timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());

                                date.setText(timeStamp);

                                SharedPreferences sharedPreferences = getSharedPreferences("GOLD", MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("goldrate", ratestring);
                                editor.putString("date", timeStamp);
                                editor.commit();


                                progress.dismiss();


                            }
                        });


                    } catch (Throwable e) {

                        e.printStackTrace();
                        progress.dismiss();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, "NO INTERNET", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                }
            });
            t.start();
            progress.show();


        }


    }

/*
    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    */
}
